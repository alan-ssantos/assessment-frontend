import Service from '../services/CategorieService';
import View from '../views/CategorieView';
import ItemController from './ItemController';

export default class CategorieController {
  constructor() {
    this._service = new Service();
    this._view = new View('.nav');
    this._itemController = new ItemController();
    this._categories = [];
  }

  async setCategories() {
    this._categories = await this._service.fetchCategories();
    this._view.update(this._categories);
    this.addEvents();
  }

  addEvents() {
    this._links = document.querySelectorAll('.categorie');
    this._links.forEach((link) => {
      link.addEventListener('click', (e) => {
        e.preventDefault();
        this.updateItens(e.currentTarget.getAttribute('href'));
      });
    });
  }

  updateItens(link) {
    const categorie = this._categories.find((item) => item.path === link);
    this._itemController.setItems(categorie.id, categorie.name);
  }

  init() {
    this.setCategories();
  }
}
