import ItemView from '../views/ItemView';
import ItemService from '../services/ItemService';

export default class ItemController {
  constructor() {
    this._service = new ItemService();
    this._view = new ItemView('.item-section');
    this._items = [];
  }

  async setItems(categorie, categorieName) {
    this._items = await this._service.fetchItems(categorie);
    this._view.update(this._items, (categorieName || 'Calçados'));
  }
}
