export default class Categorie {
  constructor(id, name, path) {
    this._id = id;
    this._name = name;
    this._path = path;

    Object.freeze(this);
  }

  get id() {
    return this._id;
  }

  get name() {
    return this._name;
  }

  get path() {
    return this._path;
  }
}
