export default class Item {
  constructor(id, sku, path, name, image, price, filter) {
    this._id = id;
    this._sku = sku;
    this._path = path;
    this._name = name;
    this._image = image;
    this._price = price;
    this._filter = filter;

    Object.freeze(this);
  }

  get id() {
    return this._id;
  }

  get sku() {
    return this._sku;
  }

  get path() {
    return this._path;
  }

  get name() {
    return this._name;
  }

  get image() {
    return this._image;
  }

  get price() {
    return this._price;
  }

  get filter() {
    return this._filter;
  }
}
