export default class CategorieView {
  constructor(element) {
    this._element = document.querySelector(element);
  }

  _template(categories) {
    this.template = `<li class="nav-item">
                        <a class="nav-link active" href="#">Página inicial</a>
                      </li>

                      ${categories.map((c) => (`<li class="nav-item">
                      <a class="nav-link categorie" href="${c.path}">${c.name}</a>
                      </li>`)).join('')}

                      <li class="nav-item">
                      <a class="nav-link" href="#">Contatos</a>
                      </li>`;

    return this.template;
  }

  update(categories) {
    this._element.innerHTML = this._template(categories);
  }
}
