export default class ItemView {
  constructor(element) {
    this._element = document.querySelector(element);
    this._title = document.querySelector('#title');
  }

  _template(items) {
    const formatter = new Intl.NumberFormat('pt-BR', {
      style: 'currency',
      currency: 'BRL',
    });

    this.template = `${items.map((i) => (
      `<div class="col-lg-3 col-md-4 col-sm-6 p-0 item">
      <div class="card">
      <img src="${i.image}" class="card-img-top" alt="${i.name}">
      <div class="card-body">
      <h5 class="card-title">${i.name}</h5>
      <p class="card-price">${formatter.format(i.price)}</p>
      <a href="${i.path}" class="btn btn-secondary card-btn">Comprar</a>
      </div>
      </div>
      </div>`)).join('')}`;

    return this.template;
  }

  update(items, categorieName) {
    this._element.innerHTML = this._template(items);
    this._title.innerText = categorieName;
  }
}
