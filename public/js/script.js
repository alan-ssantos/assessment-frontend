import CategorieController from './controllers/CategorieController';
import ItemController from './controllers/ItemController';
// import NavigationController from './controllers/NavigationController';

const categorieController = new CategorieController();
categorieController.init();

const itemController = new ItemController();
itemController.setItems(3);

// const navigationController = new NavigationController();
// navigationController.init();
