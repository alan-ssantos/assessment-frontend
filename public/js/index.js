/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./public/js/script.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./public/js/controllers/CategorieController.js":
/*!******************************************************!*\
  !*** ./public/js/controllers/CategorieController.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return CategorieController; });\n/* harmony import */ var _services_CategorieService__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../services/CategorieService */ \"./public/js/services/CategorieService.js\");\n/* harmony import */ var _views_CategorieView__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../views/CategorieView */ \"./public/js/views/CategorieView.js\");\n/* harmony import */ var _ItemController__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ItemController */ \"./public/js/controllers/ItemController.js\");\n\n\n\n\nclass CategorieController {\n  constructor() {\n    this._service = new _services_CategorieService__WEBPACK_IMPORTED_MODULE_0__[\"default\"]();\n    this._view = new _views_CategorieView__WEBPACK_IMPORTED_MODULE_1__[\"default\"]('.nav');\n    this._itemController = new _ItemController__WEBPACK_IMPORTED_MODULE_2__[\"default\"]();\n    this._categories = [];\n  }\n\n  async setCategories() {\n    this._categories = await this._service.fetchCategories();\n    this._view.update(this._categories);\n    this.addEvents();\n  }\n\n  addEvents() {\n    this._links = document.querySelectorAll('.categorie');\n    this._links.forEach((link) => {\n      link.addEventListener('click', (e) => {\n        e.preventDefault();\n        this.updateItens(e.currentTarget.getAttribute('href'));\n      });\n    });\n  }\n\n  updateItens(link) {\n    const categorie = this._categories.find((item) => item.path === link);\n    this._itemController.setItems(categorie.id, categorie.name);\n  }\n\n  init() {\n    this.setCategories();\n  }\n}\n\n\n//# sourceURL=webpack:///./public/js/controllers/CategorieController.js?");

/***/ }),

/***/ "./public/js/controllers/ItemController.js":
/*!*************************************************!*\
  !*** ./public/js/controllers/ItemController.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return ItemController; });\n/* harmony import */ var _views_ItemView__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../views/ItemView */ \"./public/js/views/ItemView.js\");\n/* harmony import */ var _services_ItemService__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/ItemService */ \"./public/js/services/ItemService.js\");\n\n\n\nclass ItemController {\n  constructor() {\n    this._service = new _services_ItemService__WEBPACK_IMPORTED_MODULE_1__[\"default\"]();\n    this._view = new _views_ItemView__WEBPACK_IMPORTED_MODULE_0__[\"default\"]('.item-section');\n    this._items = [];\n  }\n\n  async setItems(categorie, categorieName) {\n    this._items = await this._service.fetchItems(categorie);\n    this._view.update(this._items, (categorieName || 'Calçados'));\n  }\n}\n\n\n//# sourceURL=webpack:///./public/js/controllers/ItemController.js?");

/***/ }),

/***/ "./public/js/models/Categorie.js":
/*!***************************************!*\
  !*** ./public/js/models/Categorie.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return Categorie; });\nclass Categorie {\n  constructor(id, name, path) {\n    this._id = id;\n    this._name = name;\n    this._path = path;\n\n    Object.freeze(this);\n  }\n\n  get id() {\n    return this._id;\n  }\n\n  get name() {\n    return this._name;\n  }\n\n  get path() {\n    return this._path;\n  }\n}\n\n\n//# sourceURL=webpack:///./public/js/models/Categorie.js?");

/***/ }),

/***/ "./public/js/models/Item.js":
/*!**********************************!*\
  !*** ./public/js/models/Item.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return Item; });\nclass Item {\n  constructor(id, sku, path, name, image, price, filter) {\n    this._id = id;\n    this._sku = sku;\n    this._path = path;\n    this._name = name;\n    this._image = image;\n    this._price = price;\n    this._filter = filter;\n\n    Object.freeze(this);\n  }\n\n  get id() {\n    return this._id;\n  }\n\n  get sku() {\n    return this._sku;\n  }\n\n  get path() {\n    return this._path;\n  }\n\n  get name() {\n    return this._name;\n  }\n\n  get image() {\n    return this._image;\n  }\n\n  get price() {\n    return this._price;\n  }\n\n  get filter() {\n    return this._filter;\n  }\n}\n\n\n//# sourceURL=webpack:///./public/js/models/Item.js?");

/***/ }),

/***/ "./public/js/script.js":
/*!*****************************!*\
  !*** ./public/js/script.js ***!
  \*****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _controllers_CategorieController__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./controllers/CategorieController */ \"./public/js/controllers/CategorieController.js\");\n/* harmony import */ var _controllers_ItemController__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./controllers/ItemController */ \"./public/js/controllers/ItemController.js\");\n\n\n// import NavigationController from './controllers/NavigationController';\n\nconst categorieController = new _controllers_CategorieController__WEBPACK_IMPORTED_MODULE_0__[\"default\"]();\ncategorieController.init();\n\nconst itemController = new _controllers_ItemController__WEBPACK_IMPORTED_MODULE_1__[\"default\"]();\nitemController.setItems(3);\n\n// const navigationController = new NavigationController();\n// navigationController.init();\n\n\n//# sourceURL=webpack:///./public/js/script.js?");

/***/ }),

/***/ "./public/js/services/CategorieService.js":
/*!************************************************!*\
  !*** ./public/js/services/CategorieService.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return CategorieService; });\n/* harmony import */ var _models_Categorie__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../models/Categorie */ \"./public/js/models/Categorie.js\");\n\n\nclass CategorieService {\n  async fetchCategories() {\n    const response = await fetch('./api/V1/categories/list');\n    this.json = await (await response).json();\n    return this.json.items.map((e) => (new _models_Categorie__WEBPACK_IMPORTED_MODULE_0__[\"default\"](e.id, e.name, e.path)));\n  }\n}\n\n\n//# sourceURL=webpack:///./public/js/services/CategorieService.js?");

/***/ }),

/***/ "./public/js/services/ItemService.js":
/*!*******************************************!*\
  !*** ./public/js/services/ItemService.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return ItemService; });\n/* harmony import */ var _models_Item__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../models/Item */ \"./public/js/models/Item.js\");\n\n\nclass ItemService {\n  async fetchItems(categorie) {\n    const URL = './api/V1/categories/';\n    const response = await fetch(URL.concat(categorie));\n    const json = await (await response).json();\n\n    this._items = json.items.map((i) => (new _models_Item__WEBPACK_IMPORTED_MODULE_0__[\"default\"](\n      i.id, i.sku, i.path, i.name, i.image, i.price, i.filter,\n    )));\n\n    return this._items;\n  }\n}\n\n\n//# sourceURL=webpack:///./public/js/services/ItemService.js?");

/***/ }),

/***/ "./public/js/views/CategorieView.js":
/*!******************************************!*\
  !*** ./public/js/views/CategorieView.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return CategorieView; });\nclass CategorieView {\n  constructor(element) {\n    this._element = document.querySelector(element);\n  }\n\n  _template(categories) {\n    this.template = `<li class=\"nav-item\">\n                        <a class=\"nav-link active\" href=\"#\">Página inicial</a>\n                      </li>\n\n                      ${categories.map((c) => (`<li class=\"nav-item\">\n                      <a class=\"nav-link categorie\" href=\"${c.path}\">${c.name}</a>\n                      </li>`)).join('')}\n\n                      <li class=\"nav-item\">\n                      <a class=\"nav-link\" href=\"#\">Contatos</a>\n                      </li>`;\n\n    return this.template;\n  }\n\n  update(categories) {\n    this._element.innerHTML = this._template(categories);\n  }\n}\n\n\n//# sourceURL=webpack:///./public/js/views/CategorieView.js?");

/***/ }),

/***/ "./public/js/views/ItemView.js":
/*!*************************************!*\
  !*** ./public/js/views/ItemView.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return ItemView; });\nclass ItemView {\n  constructor(element) {\n    this._element = document.querySelector(element);\n    this._title = document.querySelector('#title');\n  }\n\n  _template(items) {\n    const formatter = new Intl.NumberFormat('pt-BR', {\n      style: 'currency',\n      currency: 'BRL',\n    });\n\n    this.template = `${items.map((i) => (\n      `<div class=\"col-lg-3 col-md-4 col-sm-6 p-0 item\">\n      <div class=\"card\">\n      <img src=\"${i.image}\" class=\"card-img-top\" alt=\"${i.name}\">\n      <div class=\"card-body\">\n      <h5 class=\"card-title\">${i.name}</h5>\n      <p class=\"card-price\">${formatter.format(i.price)}</p>\n      <a href=\"${i.path}\" class=\"btn btn-secondary card-btn\">Comprar</a>\n      </div>\n      </div>\n      </div>`)).join('')}`;\n\n    return this.template;\n  }\n\n  update(items, categorieName) {\n    this._element.innerHTML = this._template(items);\n    this._title.innerText = categorieName;\n  }\n}\n\n\n//# sourceURL=webpack:///./public/js/views/ItemView.js?");

/***/ })

/******/ });