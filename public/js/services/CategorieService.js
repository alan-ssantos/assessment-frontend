import Categorie from '../models/Categorie';

export default class CategorieService {
  async fetchCategories() {
    const response = await fetch('./api/V1/categories/list');
    this.json = await (await response).json();
    return this.json.items.map((e) => (new Categorie(e.id, e.name, e.path)));
  }
}
