import Item from '../models/Item';

export default class ItemService {
  async fetchItems(categorie) {
    const URL = './api/V1/categories/';
    const response = await fetch(URL.concat(categorie));
    const json = await (await response).json();

    this._items = json.items.map((i) => (new Item(
      i.id, i.sku, i.path, i.name, i.image, i.price, i.filter,
    )));

    return this._items;
  }
}
